package com.andrei1058.vipfeatures.api;

import java.util.UUID;

public interface DatabaseAdapter {

    /**
     * Check if database is remove, like MySQL
     *
     * @since 0.1
     */
    boolean isRemote();

    /**
     * Close database connection
     *
     * @since 0.1
     */
    void close();

    /**
     * Create arrow trails table if not exists
     *
     * @since 0.1
     */
    void setupTrailsTable();


    /**
     * Create arrow spells table if not exists
     *
     * @since 0.1
     */
    void setupSpellsTable();

    /**
     * Create player particles table if not exists
     *
     * @since 0.1
     */
    void setupParticlesTable();

    /**
     * Create boosters table if not exists
     *
     * @since 0.1
     */
    void setupBoostersTable();

    /**
     * Get selected trails type
     *
     * @since 0.1
     */
    TrailType getSelectedTrails(UUID uuid);

    /**
     * Get selected spells type
     *
     * @since 0.1
     */
    SpellType getSelectedSpells(UUID uuid);

    /**
     * Get selected particles type
     *
     * @since 0.1
     */
    ParticleType getSelectedParticles(UUID uuid);

    /**
     * Get selected booster type
     *
     * @since 0.1
     */
    BoosterType getSelectedBooster(UUID uuid);

    /**
     * Set selected booster for a player
     * @since 0.1
     */
    void setSelectedBooster(UUID uuid, BoosterType boosterType);

    /**
     * Set selected particles for a player
     * @since 0.1
     */
    void setSelectedParticles(UUID uuid, ParticleType particlesType);

    /**
     * Set selected spells for a player
     * @since 0.1
     */
    void setSelectedSpells(UUID uuid, SpellType spellType);

    /**
     * Set selected trails for a player
     * @since 0.1
     */
    void setSelectedTrails(UUID uuid, TrailType trailType);

}
