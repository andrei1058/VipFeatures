package com.andrei1058.vipfeatures.items;

import com.andrei1058.vipfeatures.VipFeatures;
import com.andrei1058.vipfeatures.configuration.Config;
import com.andrei1058.vipfeatures.configuration.Language;
import com.andrei1058.vipfeatures.exceptions.InvalidMaterial;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ItemCreator {

    /**
     * Give a item to interact with so it will open a GUI
     * This will check if the player have the permission and also
     * if the item is enabled in config before giving it
     *
     * @param configPath item path in config file. Use ConfigPath class.
     * @param langPath   language path for given item. Use Messages class.
     * @param itemType   is used to detect the item when you interact with it
     */
    public static void givePlayerItemStack(Player p, Config config, String configPath, String langPath, ItemType itemType, String permission) {
        if (!config.getBoolean(configPath + ".enable")) return;
        if (!p.hasPermission(permission)) return;
        try {
            p.getInventory().setItem(config.getInt(configPath+".slot"), getItemStack(config, configPath, langPath, Language.getPlayerLanguage(p), itemType));
        } catch (InvalidMaterial invalidMaterial) {
            invalidMaterial.printStackTrace();
        }
    }

    /**
     * Save a item to the given config
     *
     * @param configPath item path in config file. Use ConfigPath class.
     * @param langPath   language path for given item. Use Messages class.
     */
    public static void saveItemStackToConfig(Config config, String configPath, String langPath, String displayName, List<String> lore, boolean enchant, int slot, int data, Material material) {
        if (!config.exists(configPath + ".material")) config.set(configPath + ".material", material.toString());
        if (!config.exists(configPath + ".data")) config.set(configPath + ".data", data);
        if (!config.exists(configPath + ".enchanted")) config.set(configPath + ".enchanted", enchant);
        if (!config.exists(configPath + ".slot")) config.set(configPath + ".slot", slot);
        if (!config.exists(configPath + ".enable")) config.set(configPath + ".enable", true);
        Language.saveIfNotExists(langPath + ".name", displayName);
        Language.saveIfNotExists(langPath + ".lore", lore);
    }

    /**
     * Create an ItemStack for
     *
     * @param configPath item path in config file. Use ConfigPath class.
     * @param langPath   language path for given item. Use Messages class.
     * @param itemType   is used to detect the item when you interact with it
     */
    public static ItemStack getItemStack(Config config, String configPath, String langPath, Language language, ItemType itemType) throws InvalidMaterial {
        ItemStack i;

        try {
            i = new ItemStack(Material.valueOf(config.getYml().getString(configPath + ".material")), 1, (short) config.getInt(configPath + ".data"));
        } catch (Exception ex) {
            throw new InvalidMaterial("Invalid material at: "+ configPath);
        }

        ItemMeta im = i.getItemMeta();

        if (config.getBoolean(configPath + ".enchanted")) {
            im.addEnchant(Enchantment.LUCK, 1, true);
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }

        im.setDisplayName(language.m(langPath + ".name"));
        im.setLore(language.l(langPath + ".lore"));
        i.setItemMeta(im);
        return VipFeatures.getItemStackSupport().addTag(i, VipFeatures.INTERACT_TAG, itemType.toString());
    }

    public enum ItemType {VIPFEATURES_MAIN_GUI_OPENER}
}
