package com.andrei1058.vipfeatures.customgui;

import com.andrei1058.vipfeatures.configuration.Config;
import com.andrei1058.vipfeatures.configuration.Language;
import org.bukkit.entity.Player;

public abstract class CustomGUI {

    private int size;
    private String name, displayName;
    private Config cm;
    private Language lang;

    /**
     * Create a custom GUI per language
     *
     * @param name ID name.
     * @param size GUI size.
     * @param cm   Config instance where to get options.
     * @param lang Language instance where to get translations.
     */
    public CustomGUI(String name, int size, Config cm, Language lang) {
        this.name = name;
        this.size = size;
        this.cm = cm;
        this.lang = lang;
    }

    public abstract void addItem(String itemPath, String langPath);

    public abstract void open(Player p);

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Config getCm() {
        return cm;
    }

    public Language getLang() {
        return lang;
    }

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }
}
