package com.andrei1058.vipfeatures.customgui;

import com.andrei1058.vipfeatures.VipFeatures;
import com.andrei1058.vipfeatures.api.BoosterType;
import com.andrei1058.vipfeatures.api.ParticleType;
import com.andrei1058.vipfeatures.api.SpellType;
import com.andrei1058.vipfeatures.api.TrailType;
import com.andrei1058.vipfeatures.configuration.Config;
import com.andrei1058.vipfeatures.configuration.Language;
import com.andrei1058.vipfeatures.configuration.Messages;
import com.andrei1058.vipfeatures.perks.BoostersManager;
import com.andrei1058.vipfeatures.perks.ParticleManager;
import com.andrei1058.vipfeatures.perks.SpellsManager;
import com.andrei1058.vipfeatures.perks.TrailsManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class PlayerGUI extends CustomGUI {

    private final HashMap<String, String> paths = new HashMap<>();

    public PlayerGUI(String name, int size, Config cm, Language lang) {
        super(name, size, cm, lang);
        setDisplayName(lang.m(name + ".name"));
    }


    /**
     * Available placeholders {player}, {status}.
     * @param p player.
     */
    public void open(final Player p) {
        Inventory inv = Bukkit.createInventory(null, getSize(), getDisplayName().replace("{player}", p.getName()));
        Bukkit.getScheduler().runTaskAsynchronously(VipFeatures.plugin, ()-> {
            String selected = "", permission = "";
            if (getName().equalsIgnoreCase("particlesGUI")) {
                ParticleType particlesType = ParticleManager.getINSTANCE().getPlayerParticles(p);
                selected = particlesType.toString();
                permission = particlesType.getPermission();
            } else if (getName().contains("spellsGUI")) {
                SpellType spellType = SpellsManager.getINSTANCE().getPlayerSpells(p);
                selected = spellType.toString();
                permission = spellType.getPermission();
            } else if (getName().equalsIgnoreCase("boostersGUI")) {
                BoosterType boosterType = BoostersManager.getINSTANCE().getPlayerBooster(p);
                selected = boosterType.toString();
                permission = boosterType.getPermission();
            } else if (getName().equalsIgnoreCase("trailsGUI")){
                TrailType trailType = TrailsManager.getINSTANCE().getPlayerTrails(p);
                selected = trailType.toString();
                permission = trailType.getPermission();
            }
            for (Map.Entry<String, String> e : paths.entrySet()) {
                String itemName = e.getValue();
                if (itemName.contains(".")) {
                    String[] temp = itemName.split("\\.");
                    itemName = itemName.split("\\.")[temp.length - 1];
                }
                addItemStack(e.getKey(), e.getValue(), inv, selected.equals(itemName), p.hasPermission(permission));
            }
            Bukkit.getScheduler().runTask(VipFeatures.plugin, () -> p.openInventory(inv));
        });
    }

    public void addItem(String itemPath, String langPath) {
        if (!getCm().exists(itemPath)) return;
        if (!getCm().exists(itemPath + ".material")) return;
        try {
            Material.valueOf(getCm().getYml().getString(itemPath + ".material"));
        } catch (Exception e) {
            return;
        }
        if (!getCm().exists(itemPath + ".slot")) return;
        if (!getLang().exists(langPath)) {
            getLang().set(langPath + ".name", "&cNAME NOT SET");
            getLang().set(langPath + ".lore", Arrays.asList("", "&7Lore not set!"));
        }
        paths.put(itemPath, langPath);
    }

    public void addItemStack(String itemPath, String langPath, Inventory inv, boolean selected, boolean permission) {
        String name = "";
        if (getLang().exists(langPath + ".name")) name = getLang().m(langPath + ".name");

        // If selected make its name bold
        if (selected){
            name = ChatColor.BOLD + name;
        }
        boolean enchanted = false;
        if (getCm().exists(itemPath + ".enchanted")) enchanted = getCm().getBoolean(itemPath + ".enchanted");
        List<String> newLore = new ArrayList<>();
        if (getLang().exists(langPath + ".lore")) {
            for (String s : getLang().l(langPath + ".lore")) {
                if (s.contains("{status}")) {
                    if (!permission) {
                        s = s.replace("{status}", getLang().m(Messages.STATUS_REPLACEMENT_NO_PERMISSION));
                    } else if (selected) {
                        s = s.replace("{status}", getLang().m(Messages.STATUS_REPLACEMENT_SELECTED));
                    } else {
                        s = s.replace("{status}", getLang().m(Messages.STATUS_REPLACEMENT_CLICK_TO_SELECT));
                    }
                }
                newLore.add(s);
            }
        }
        inv.setItem(getCm().getInt(itemPath + ".slot"), createItemStack(name, enchanted, newLore,
                Material.valueOf(getCm().getYml().getString(itemPath + ".material")), getCm().exists(itemPath + ".data") ? getCm().getInt(itemPath + ".data")
                        : 0, getCm().exists(itemPath + ".amount") ? getCm().getInt(itemPath + ".amount") : 1));
    }

    public static void createItem(Config cm, String itemPath, Material material, int data, int amount, int slot, boolean enchanted,
                                  String name, List<String> lore, String langPath) {
        if (!cm.exists(itemPath + ".material")) cm.set(itemPath + ".material", material.toString());
        if (!(data == 0 && cm.exists(itemPath + ".data"))) cm.set(itemPath + ".data", data);
        if (amount > 1 && !cm.exists(itemPath + ".amount")) cm.set(itemPath + ".amount", amount);
        if (!cm.exists(itemPath + ".slot")) cm.set(itemPath + ".slot", slot);
        if (enchanted && !cm.exists(itemPath + ".enchanted")) cm.set(itemPath + ".enchanted", enchanted);
        for (Language l : Language.getLanguages()) {
            if (!l.exists(langPath + ".name")) l.set(langPath + ".name", name);
            if (lore != null) {
                if (!l.exists(langPath + ".lore")) l.set(langPath + ".lore", lore);
            }
        }
    }

    public static void setupGUI(Config cm, String path, String name, String displayName, int size) {
        if (!path.isEmpty()) path = path + ".";
        if (!cm.exists(path + name + ".invSize")) {
            cm.set(path + name + ".invSize", size);
        }
        for (Language l : Language.getLanguages()) {
            if (!l.exists(name + ".name")) l.set(name + ".name", displayName);
        }
        for (Language l : Language.getLanguages()) {
            l.getLangGUIs().add(new PlayerGUI(name, size, cm, l));
        }
    }

    public static ItemStack createItemStack(String name, boolean enchanted, List<String> lore, Material material, int data, int amount) {
        ItemStack i = new ItemStack(material, amount, (short) data);
        ItemMeta im = i.getItemMeta();
        if (enchanted) {
            im.addEnchant(Enchantment.LUCK, 1, true);
        }
        im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        im.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);

        im.setDisplayName(name);
        if (lore != null) im.setLore(lore);
        i.setItemMeta(im);
        return i;
    }
}
