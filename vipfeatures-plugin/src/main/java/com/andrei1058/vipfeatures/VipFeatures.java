package com.andrei1058.vipfeatures;

import com.andrei1058.spigot.versionsupport.CommandSupport;
import com.andrei1058.spigot.versionsupport.ItemStackSupport;
import com.andrei1058.spigot.versionsupport.ParticleSupport;
import com.andrei1058.vipfeatures.api.IVipFeatures;
import com.andrei1058.vipfeatures.api.MiniGameAlreadyRegistered;
import com.andrei1058.vipfeatures.commands.VipFeaturesCMD;
import com.andrei1058.vipfeatures.configuration.*;
import com.andrei1058.vipfeatures.api.DatabaseAdapter;
import com.andrei1058.vipfeatures.database.MySQL;
import com.andrei1058.vipfeatures.api.MiniGame;
import com.andrei1058.vipfeatures.database.SQLite;
import com.andrei1058.vipfeatures.items.ItemCreator;
import com.andrei1058.vipfeatures.listeners.GeneralListeners;
import com.andrei1058.vipfeatures.perks.BoostersManager;
import com.andrei1058.vipfeatures.perks.ParticleManager;
import com.andrei1058.vipfeatures.perks.SpellsManager;
import com.andrei1058.vipfeatures.perks.TrailsManager;
import com.andrei1058.vipfeatures.tasks.ParticlesTask;
import com.andrei1058.vipfeatures.tasks.TrailsTask;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.logging.Level;

public class VipFeatures extends JavaPlugin implements IVipFeatures {

    public static VipFeatures plugin;
    public static Config config, mainGUI, trailsGUI, spellsGUI, particlesGUI, boostersGUI;
    public static DatabaseAdapter databaseAdapter;
    private static ParticleSupport particleSupport;
    private static ItemStackSupport itemStackSupport;
    public static final String INTERACT_TAG = "avf-in";

    // Loaded support for mini-games
    private static final List<MiniGame> miniGames = new ArrayList<>();

    @Override
    public void onEnable() {
        plugin = this;
        String version = Bukkit.getServer().getClass().getName().split("\\.")[3];

        particleSupport = ParticleSupport.SupportBuilder.load();
        itemStackSupport = ItemStackSupport.SupportBuilder.load();
        CommandSupport commandSupport = CommandSupport.SupportBuilder.load();

        if (particleSupport == null || itemStackSupport == null || commandSupport == null) {
            Bukkit.getPluginManager().disablePlugin(plugin);
            getLogger().log(Level.SEVERE, "Your server version is not supported: " + version);
            return;
        }

        Bukkit.getServicesManager().register(IVipFeatures.class, this, this, ServicePriority.Normal);
        Config.setupConfiguration();
        Language.initializeLanguages();
        Config.setupMainGUI();
        Config.setupTrailsGUI();
        Config.setupSpellsGUI();
        Config.setupParticlesGUI();
        Config.setupBoostersGUI();
        if (config.getBoolean(ConfigPath.DATABASE_ENABLE)) {
            Bukkit.getScheduler().runTaskLaterAsynchronously(this, () -> {
                databaseAdapter = new MySQL();
            }, 10L);
        } else {
            databaseAdapter = new SQLite();
        }
        commandSupport.registerCommand(this, "vipfeatures", new VipFeaturesCMD("vipfeatures", Collections.singletonList("vf"), "Opens a GUI"));
        new TrailsTask().runTaskTimerAsynchronously(this, 0L, 1L);
        new ParticlesTask().runTaskTimerAsynchronously(this, 0L, 6L);
        Bukkit.getPluginManager().registerEvents(new GeneralListeners(), this);

        Metrics metrics = new Metrics(this, 8977);
        metrics.addCustomChart(new Metrics.AdvancedPie("mini_games", () -> {
            Map<String, Integer> valueMap = new HashMap<>();
            for (MiniGame mg : getMiniGames()) {
                valueMap.put(mg.getOwner().getName(), 1);
            }
            return valueMap;
        }));

        metrics.addCustomChart(new Metrics.SimplePie("default_language", () -> config.getYml().getString(ConfigPath.DEFAULT_LANGUAGE)));

        metrics.addCustomChart(new Metrics.SimplePie("database_adapter", () -> databaseAdapter.getClass().getName()));
    }


    public static List<MiniGame> getMiniGames() {
        return miniGames;
    }

    @Override
    public void registerMiniGame(MiniGame miniGame) throws MiniGameAlreadyRegistered {
        for (MiniGame mg : miniGames) {
            if (mg.getOwner() == miniGame.getOwner()) throw new MiniGameAlreadyRegistered(miniGame.getOwner());
        }
        miniGames.add(miniGame);
    }

    @Override
    public void givePlayerItemStack(Player player) {
        ItemCreator.givePlayerItemStack(player, VipFeatures.config, ConfigPath.MAIN_GUI_ITEMSTACK, Messages.MAIN_GUI_ITEM_PATH, ItemCreator.ItemType.VIPFEATURES_MAIN_GUI_OPENER, Permissions.PERMISSION_RECEIVE_AND_OPEN_MAIN_GUI);
    }

    @Override
    public ParticlesUtil getParticlesUtil() {
        return ParticleManager.getINSTANCE();
    }

    @Override
    public VersionUtil getVersionUtil() {
        return VipFeatures::getForCurrentVersion;
    }

    @Override
    public SpellsUtil getSpellsUtil() {
        return SpellsManager.getINSTANCE();
    }

    @Override
    public TrailsUtil getTrailsUtil() {
        return TrailsManager.getINSTANCE();
    }

    @Override
    public BoostersUtil getBoostersUtil() {
        return BoostersManager.getINSTANCE();
    }

    @Override
    public Plugin getVipFeatures() {
        return this;
    }

    @Override
    public void setDatabaseAdapter(DatabaseAdapter newAdapter) {
        if (newAdapter == null) {
            databaseAdapter = new SQLite();
            return;
        }
        databaseAdapter = newAdapter;
    }

    @Override
    public DatabaseAdapter getDatabaseAdapter() {
        return databaseAdapter;
    }

    public static String getForCurrentVersion(String v18, String v12, String v13) {
        return getParticleSupport().getForVersion(v18, v12, v12, v13);
    }

    public static ParticleSupport getParticleSupport() {
        return particleSupport;
    }

    public static ItemStackSupport getItemStackSupport() {
        return itemStackSupport;
    }
}
