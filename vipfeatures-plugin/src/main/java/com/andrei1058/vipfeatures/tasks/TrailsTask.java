package com.andrei1058.vipfeatures.tasks;

import com.andrei1058.vipfeatures.VipFeatures;
import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

public class TrailsTask extends BukkitRunnable {

    // Particles that needs to have particles
    private static final ConcurrentLinkedQueue<Arrow> projectiles = new ConcurrentLinkedQueue<>();

    /**
     * Get projectiles that have active effects.
     */
    public static ConcurrentLinkedQueue<Arrow> getProjectiles() {
        return projectiles;
    }

    @Override
    public void run() {
        for (Arrow a : new ArrayList<>(getProjectiles())) {
            if (a.isDead()) continue;
            Location l = a.getLocation();
            VipFeatures.getParticleSupport().spawnParticle(l.getWorld(), (float) l.getX(), (float) l.getY(), (float) l.getZ(), a.getMetadata("trailName").get(0).asString());
        }
    }
}
