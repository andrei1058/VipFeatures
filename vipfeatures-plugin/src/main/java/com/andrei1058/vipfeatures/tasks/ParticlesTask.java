package com.andrei1058.vipfeatures.tasks;

import com.andrei1058.vipfeatures.VipFeatures;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ParticlesTask extends BukkitRunnable {

    // Players who need to have particles on head
    private static final ConcurrentHashMap<Player, String> particlesOnHead = new ConcurrentHashMap<>();

    /**
     * Get players with particles that needs to be spawned on head.
     * Those particles are managed by the main task and here are not included some particle types.
     */
    public static ConcurrentHashMap<Player, String> getParticlesOnHead() {
        return particlesOnHead;
    }

    @Override
    public void run() {
        for (Map.Entry<Player, String> e : particlesOnHead.entrySet()) {
            if (e.getKey().hasPotionEffect(PotionEffectType.INVISIBILITY)) continue;
            Location l = e.getKey().getLocation().add(0, 2.3, 0);
            VipFeatures.getParticleSupport().spawnParticle(l.getWorld(), (float) l.getX(), (float) l.getY(), (float) l.getZ(), e.getValue());
        }
    }
}
