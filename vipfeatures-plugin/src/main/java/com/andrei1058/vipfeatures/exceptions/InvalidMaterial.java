package com.andrei1058.vipfeatures.exceptions;

public class InvalidMaterial extends Exception {

    public InvalidMaterial(String message){
        super(message);
    }
}
