package com.andrei1058.vipfeatures.perks;

import com.andrei1058.vipfeatures.api.IVipFeatures;
import com.andrei1058.vipfeatures.api.TrailType;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class TrailsManager implements IVipFeatures.TrailsUtil {

    private static TrailsManager INSTANCE;

    TrailsManager() {}

    // Contains trails preferences
    private final HashMap<Player, TrailType> trailByPlayer = new HashMap<>();

    /**
     * Get the preferences list.
     */
    public HashMap<Player, TrailType> getTrailByPlayer() {
        return trailByPlayer;
    }

    /**
     * Check if a player have bow trails preferences.
     *
     * @param p player.
     * @return true if the player have bow trails.
     */
    public boolean hasTrails(Player p) {
        return getTrailByPlayer().containsKey(p);
    }

    /**
     * Get a player trails preferences.
     *
     * @param p player.
     * @return the trail type, type none if doesn't have preferences
     * @see TrailType
     */
    public TrailType getPlayerTrails(Player p) {
        if (hasTrails(p)) return getTrailByPlayer().get(p);
        return TrailType.NONE;
    }

    @Override
    public void togglePlayerTrails(Player player, TrailType type) {
        setPlayerTrails(player, type);
    }

    /**
     * Save trails preferences for a player.
     * If the player is already in the preferences list it will be replaced.
     *
     * @param p         player.
     * @param trailType trail.
     */
    public void setPlayerTrails(Player p, TrailType trailType) {
        if (!p.isOnline()) return;
        if (trailType == TrailType.NONE) {
            if (hasTrails(p)) getTrailByPlayer().remove(p);
            return;
        }
        if (!hasPermission(p, trailType)) return;
        if (getTrailByPlayer().containsKey(p)) getTrailByPlayer().replace(p, trailType);
        else getTrailByPlayer().put(p, trailType);
    }

    /**
     * Check permission.
     *
     * @param p         player.
     * @param trailType trail.
     */
    private static boolean hasPermission(Player p, TrailType trailType) {
        return p.hasPermission("vipfeatures.*") || p.hasPermission("vipfeatures.trails.*") || p.hasPermission(trailType.getPermission());
    }

    public static TrailsManager getINSTANCE() {
        if (INSTANCE == null){
            INSTANCE = new TrailsManager();
        }
        return INSTANCE;
    }
}
