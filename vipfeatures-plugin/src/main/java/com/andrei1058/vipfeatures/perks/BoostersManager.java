package com.andrei1058.vipfeatures.perks;

import com.andrei1058.vipfeatures.VipFeatures;
import com.andrei1058.vipfeatures.api.BoosterType;
import com.andrei1058.vipfeatures.api.IVipFeatures;
import com.andrei1058.vipfeatures.configuration.Language;
import com.andrei1058.vipfeatures.configuration.Messages;
import com.andrei1058.vipfeatures.api.MiniGame;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class BoostersManager implements IVipFeatures.BoostersUtil {

    private static BoostersManager INSTANCE;

    BoostersManager(){}

    private final HashMap<Player, BoosterType> playerBooster = new HashMap<>();

    /**
     * Get the preferences list.
     */
    public HashMap<Player, BoosterType> getPlayerBoosters() {
        return playerBooster;
    }

    /**
     * Check if a player has an active booster.
     *
     * @param p player.
     * @return true if the player has a booster.
     */
    public boolean hasBooster(Player p) {
        return getPlayerBoosters().containsKey(p);
    }

    /**
     * Get integer multiplier.
     *
     * @param p player.
     */
    public int getMultiplier(Player p) {
        return getPlayerBooster(p).getMultiplier();
    }

    /**
     * Save booster for a player
     * If the player is already in the preferences list it will be replaced.
     *
     * @param p           player.
     * @param boosterType booster.
     * @param skipPermMsg skip permission denied msg.
     */
    public void setPlayerBooster(Player p, BoosterType boosterType, boolean skipPermMsg) {
        if (!p.isOnline()) return;
        if (boosterType == BoosterType.NONE) {
            if (hasBooster(p)) getPlayerBoosters().remove(p);
            return;
        }
        if (!hasPermission(p, boosterType)) return;
        for (MiniGame m : VipFeatures.getMiniGames()) {
            if (!m.hasBoosters()) {
                if (!skipPermMsg) {
                    p.sendMessage(Language.getMsg(p, Messages.MINIGAME_BOOSTERS_NOT_SUPPORTED).replace("{minigame}", m.getDisplayName()));
                }
            }
        }
        if (playerBooster.containsKey(p)) {
            playerBooster.remove(p, boosterType);
        } else {
            playerBooster.put(p, boosterType);
        }
    }

    /**
     * Check permission.
     *
     * @param p           player.
     * @param boosterType booster.
     */
    private static boolean hasPermission(Player p, BoosterType boosterType) {
        return p.hasPermission("vipfeatures.*") || p.hasPermission("vipfeatures.boosters.*") || p.hasPermission(boosterType.getPermission());
    }

    public BoosterType getPlayerBooster(Player p) {
        if (hasBooster(p)) return getPlayerBoosters().get(p);
        return BoosterType.NONE;
    }

    @Override
    public void togglePlayerBooster(Player player, BoosterType type) {
        setPlayerBooster(player, type, true);
    }

    public static BoostersManager getINSTANCE() {
        if (INSTANCE == null){
            INSTANCE = new BoostersManager();
        }
        return INSTANCE;
    }
}
