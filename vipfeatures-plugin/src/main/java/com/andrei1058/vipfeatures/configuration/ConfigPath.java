package com.andrei1058.vipfeatures.configuration;

public class ConfigPath {
    public static final String DEFAULT_LANGUAGE = "defaultLang";
    public static final String DATABASE_ENABLE = "database.enable";
    public static final String DATABASE_HOST = "database.host";
    public static final String DATABASE_PORT = "database.port";
    public static final String DATABASE_NAME = "database.name";
    public static final String DATABASE_USER = "database.user";
    public static final String DATABASE_PASSWORD = "database.pass";
    public static final String DATABASE_SSL = "database.ssl";
    public static final String BLOCK_CHANGES_DURING_THE_GAME = "disableChangesWhenPlaying";
    public static final String MAIN_GUI_ITEMSTACK = "perksItem";
}
