package com.andrei1058.vipfeatures.configuration;

public class Permissions {
    public static final String PERMISSION_RECEIVE_AND_OPEN_MAIN_GUI = "vipfeatures.open.maingui";
    public static final String TRAILS_GUI_PERMISSION = "vipfeatures.open.trails";
    public static final String SPELLS_GUI_PERMISSION = "vipfeatures.open.spells";
    public static final String PARTICLES_GUI_PERMISSION = "vipfeatures.open.particles";
    public static final String BOOSTERS_GUI_PERMISSION = "vipfeatures.open.boosters";
}
