package com.andrei1058.vipfeatures.configuration;

import com.andrei1058.vipfeatures.VipFeatures;
import com.andrei1058.vipfeatures.api.BoosterType;
import com.andrei1058.vipfeatures.api.ParticleType;
import com.andrei1058.vipfeatures.api.SpellType;
import com.andrei1058.vipfeatures.api.TrailType;
import com.andrei1058.vipfeatures.customgui.CustomGUI;
import com.andrei1058.vipfeatures.customgui.PlayerGUI;
import com.andrei1058.vipfeatures.items.ItemCreator;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Config {

    private YamlConfiguration yml;
    private final File config;

    /**
     * Create a new yml file
     *
     * @param name File name. Do not use .yml in name.
     * @param dir  Where to save the file.
     */
    public Config(String name, String dir) {
        File d = new File(dir);
        if (!d.exists()) {
            //noinspection ResultOfMethodCallIgnored
            d.mkdir();
        }
        config = new File(dir + "/" + name + ".yml");
        if (!config.exists()) {
            try {
                //noinspection ResultOfMethodCallIgnored
                config.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            yml = YamlConfiguration.loadConfiguration(config);
            VipFeatures.plugin.getLogger().info("Creating " + dir + "/" + name + ".yml");
        }
        yml = YamlConfiguration.loadConfiguration(config);
    }

    /**
     * Setup general configuration.
     * Create/ load config.yml
     */
    public static void setupConfiguration() {
        VipFeatures.config = new Config("config", "plugins/" + VipFeatures.plugin.getName());
        YamlConfiguration yml = VipFeatures.config.getYml();
        yml.options().copyDefaults(true);

        /* Configuration header */
        yml.options().header("Plugin by " + VipFeatures.plugin.getDescription().getAuthors() + "\nconfig.yml - Documentation\n\n" +
                ConfigPath.DEFAULT_LANGUAGE + ": en - Default language iso code. Make sure messages_en.yml exists!\n\n" +
                ConfigPath.DATABASE_HOST + ": localhost - Database IP or DNS.\n" +
                ConfigPath.DATABASE_PORT + ": 3306 - Database port.\n" +
                ConfigPath.DATABASE_SSL + ": false - Set to true to use a https connection.\n" +
                ConfigPath.DATABASE_NAME + ": vipFeatures - Database name.\n" +
                ConfigPath.DATABASE_USER + ": root - Database user.\n" +
                ConfigPath.DATABASE_PASSWORD + ": p4ss11 - User password.\n\n" +
                ConfigPath.BLOCK_CHANGES_DURING_THE_GAME + ": true - Set false to allow players select perks during the game.\n\n" +
                ConfigPath.MAIN_GUI_ITEMSTACK + ".enable - true if you want to give the item on join.\n" +
                ConfigPath.MAIN_GUI_ITEMSTACK + ".material - Item material.\n" +
                ConfigPath.MAIN_GUI_ITEMSTACK + ".data - Material data.\n" +
                ConfigPath.MAIN_GUI_ITEMSTACK + ".slot - The slot where to receive the item.\n" +
                ConfigPath.MAIN_GUI_ITEMSTACK + ".enchant - true if you want to have an enchanted item.\n\n");

        /* Configuration default options */
        yml.addDefault(ConfigPath.DEFAULT_LANGUAGE, "en");
        yml.addDefault(ConfigPath.DATABASE_ENABLE, false);
        yml.addDefault(ConfigPath.DATABASE_HOST, "localhost");
        yml.addDefault(ConfigPath.DATABASE_PORT, 3306);
        yml.addDefault(ConfigPath.DATABASE_SSL, false);
        yml.addDefault(ConfigPath.DATABASE_NAME, "vipFeatures");
        yml.addDefault(ConfigPath.DATABASE_USER, "root");
        yml.addDefault(ConfigPath.DATABASE_PASSWORD, "bread");
        yml.addDefault(ConfigPath.BLOCK_CHANGES_DURING_THE_GAME, true);
        /* Setup main GUI item */
        ItemCreator.saveItemStackToConfig(VipFeatures.config, ConfigPath.MAIN_GUI_ITEMSTACK, Messages.MAIN_GUI_ITEM_PATH, "&8Perks", Arrays.asList("", "&eRight-Click to open!"), false, 7, 0, Material.CHEST);
        VipFeatures.config.save();
    }

    public static void setupMainGUI() {
        Config c = new Config("mainGUI", "plugins/" + VipFeatures.plugin.getName());
        VipFeatures.mainGUI = c;
        /* Create main inventory */
        PlayerGUI.setupGUI(c, "", "mainGUI", "&8Vip Perks", 27);
        PlayerGUI.createItem(c, "mainGUI.trailsGUI", Material.BLAZE_POWDER, 0, 1, 10, false,
                "&7Arrow &eTrails", Arrays.asList("", "&fClick to open!"), "mainGUI.trailsGUI");
        PlayerGUI.createItem(c, "mainGUI.spellsGUI", Material.valueOf(VipFeatures.getForCurrentVersion("WEB", "WEB", "COBWEB")), 0, 1, 12, false,
                "&7Arrow &eSpells", Arrays.asList("", "&fClick to open!"), "mainGUI.spellsGUI");
        PlayerGUI.createItem(c, "mainGUI.particlesGUI", Material.REDSTONE, 0, 1, 14, false,
                "&7Player &eParticles", Arrays.asList("", "&fClick to open!"), "mainGUI.particlesGUI");
        PlayerGUI.createItem(c, "mainGUI.boostersGUI", Material.APPLE, 0, 1, 16, false,
                "&7Points &eBoosters", Arrays.asList("", "&fClick to open!"), "mainGUI.boostersGUI");
        for (Language l : Language.getLanguages()) {
            CustomGUI g = l.getGUI("mainGUI");
            g.addItem("mainGUI.trailsGUI", "mainGUI.trailsGUI");
            g.addItem("mainGUI.spellsGUI", "mainGUI.spellsGUI");
            g.addItem("mainGUI.particlesGUI", "mainGUI.particlesGUI");
            g.addItem("mainGUI.boostersGUI", "mainGUI.boostersGUI");
        }
    }

    public static void setupTrailsGUI() {
        Config c = new Config("trailsGUI", "plugins/" + VipFeatures.plugin.getName());
        VipFeatures.trailsGUI = c;
        /* Create trails inventory */
        PlayerGUI.setupGUI(c, "", "trailsGUI", "&8Arrow Trails", 27);
        PlayerGUI.createItem(c, "trailsGUI." + TrailType.FIRE, Material.BLAZE_POWDER, 0, 1, 11, false,
                "&cFire &7Trail", Arrays.asList("", "&f{status}"), "trailsGUI.fireTrails");
        PlayerGUI.createItem(c, "trailsGUI." + TrailType.SLIME, Material.SLIME_BALL, 0, 1, 12, false,
                "&aSlime &7Trail", Arrays.asList("", "&f{status}"), "trailsGUI.slimeTrail");
        PlayerGUI.createItem(c, "trailsGUI." + TrailType.WATER, Material.WATER_BUCKET, 0, 1, 13, false,
                "&bWater &7Trail", Arrays.asList("", "&f{status}"), "trailsGUI.waterTrail");
        PlayerGUI.createItem(c, "trailsGUI." + TrailType.NOTES, Material.JUKEBOX, 0, 1, 14, false,
                "&dNotes &7Trail", Arrays.asList("", "&f{status}"), "trailsGUI.notesTrail");
        PlayerGUI.createItem(c, "trailsGUI." + TrailType.CRYSTAL, Material.GLASS, 0, 1, 15, false,
                "&fCrystal &7Trail", Arrays.asList("", "&f{status}"), "trailsGUI.crystalTrail");
        PlayerGUI.createItem(c, "trailsGUI.backItem", Material.ARROW, 0, 1, 22, false,
                "&8Back", Arrays.asList("", "&fClick to go back!"), "trailsGUI.backItem");
        for (Language l : Language.getLanguages()) {
            CustomGUI g = l.getGUI("trailsGUI");
            g.addItem("trailsGUI." + TrailType.FIRE, "trailsGUI.fireTrails");
            g.addItem("trailsGUI." + TrailType.SLIME, "trailsGUI.slimeTrail");
            g.addItem("trailsGUI." + TrailType.WATER, "trailsGUI.waterTrail");
            g.addItem("trailsGUI." + TrailType.NONE, "trailsGUI.notesTrail");
            g.addItem("trailsGUI." + TrailType.CRYSTAL, "trailsGUI.crystalTrail");
            g.addItem("trailsGUI.backItem", "trailsGUI.backItem");
        }
    }

    public static void setupSpellsGUI() {
        Config c = new Config("spellsGUI", "plugins/" + VipFeatures.plugin.getName());
        VipFeatures.spellsGUI = c;
        /* Create spells inventory */
        PlayerGUI.setupGUI(c, "", "spellsGUI", "&8Arrow Spells", 27);
        PlayerGUI.createItem(c, "spellsGUI." + SpellType.EXPLOSIVE, Material.TNT, 0, 1, 11, false,
                "&cExplosive &7Spell", Arrays.asList("", "&f{status}"), "spellsGUI.explosiveSpell");
        PlayerGUI.createItem(c, "spellsGUI." + SpellType.FIRE, Material.BLAZE_POWDER, 0, 0, 12, false,
                "&cFire &7Spell", Arrays.asList("", "&f{status}"), "spellsGUI.fireSpell");
        PlayerGUI.createItem(c, "spellsGUI." + SpellType.WEB, Material.valueOf(VipFeatures.getForCurrentVersion("WEB", "WEB", "COBWEB")), 0, 1, 13, false,
                "&fWeb &7Spell", Arrays.asList("", "&f{status}"), "spellsGUI.webSpell");
        PlayerGUI.createItem(c, "spellsGUI." + SpellType.ZOMBIE, Material.valueOf(VipFeatures.getForCurrentVersion("SKULL_ITEM", "SKULL_ITEM", "PLAYER_HEAD")), 2, 1, 14, false,
                "&dZombie &7Spell", Arrays.asList("", "&f{status}"), "spellsGUI.zombieSpell");
        PlayerGUI.createItem(c, "spellsGUI." + SpellType.POISON, Material.POTION, 0, 1, 15, false,
                "&cPoison &7Spell", Arrays.asList("", "&f{status}"), "spellsGUI.poisonSpell");
        PlayerGUI.createItem(c, "spellsGUI.backItem", Material.ARROW, 0, 1, 22, false,
                "&8Back", Arrays.asList("", "&fClick to go back!"), "spellsGUI.backItem");
        for (Language l : Language.getLanguages()) {
            CustomGUI g = l.getGUI("spellsGUI");
            g.addItem("spellsGUI." + SpellType.EXPLOSIVE, "spellsGUI.explosiveSpell");
            g.addItem("spellsGUI." + SpellType.FIRE, "spellsGUI.fireSpell");
            g.addItem("spellsGUI." + SpellType.WEB, "spellsGUI.webSpell");
            g.addItem("spellsGUI." + SpellType.ZOMBIE, "spellsGUI.zombieSpell");
            g.addItem("spellsGUI." + SpellType.POISON, "spellsGUI.poisonSpell");
            g.addItem("spellsGUI.backItem", "spellsGUI.backItem");
        }
    }

    public static void setupParticlesGUI() {
        Config c = new Config("particlesGUI", "plugins/" + VipFeatures.plugin.getName());
        VipFeatures.particlesGUI = c;
        /* Create particles inventory */
        PlayerGUI.setupGUI(c, "", "particlesGUI", "&8Player Particles", 27);
        PlayerGUI.createItem(c, "particlesGUI." + ParticleType.SPIRAL, Material.REDSTONE, 0, 1, 10, false,
                "&eSpiral &7Particles", Arrays.asList("", "&f{status}"), "particlesGUI.spiralParticles");
        PlayerGUI.createItem(c, "particlesGUI." + ParticleType.ANGRY_VILLAGER, Material.GLOWSTONE_DUST, 0, 0, 11, false,
                "&cAngry Villager &7Particles", Arrays.asList("", "&f{status}"), "particlesGUI.angryParticles");
        PlayerGUI.createItem(c, "particlesGUI." + ParticleType.DOUBLE_WITCH, Material.ANVIL, 0, 1, 12, false,
                "&fDouble Witch &7Particles", Arrays.asList("", "&f{status}"), "particlesGUI.doubleWitchParticles");
        PlayerGUI.createItem(c, "particlesGUI." + ParticleType.NOTES, Material.JUKEBOX, 0, 1, 14, false,
                "&dMusic &7Particles", Arrays.asList("", "&f{status}"), "particlesGUI.notesParticles");
        PlayerGUI.createItem(c, "particlesGUI." + ParticleType.MAGIC, Material.valueOf(VipFeatures.getForCurrentVersion("ENCHANTMENT_TABLE", "ENCHANTMENT_TABLE", "ENCHANTING_TABLE")), 0, 1, 15, false,
                "&3Magic &7Particles", Arrays.asList("", "&f{status}"), "particlesGUI.magicParticles");
        PlayerGUI.createItem(c, "particlesGUI." + ParticleType.HAPPY_VILLAGER, Material.EMERALD, 0, 1, 16, false,
                "&aHappy Villager &7Particles", Arrays.asList("", "&f{status}"), "particlesGUI.happyParticles");
        PlayerGUI.createItem(c, "particlesGUI.backItem" + ParticleType.SPIRAL, Material.ARROW, 0, 1, 22, false,
                "&8Back", Arrays.asList("", "&fClick to go back!"), "particlesGUI.backItem");
        for (Language l : Language.getLanguages()) {
            CustomGUI g = l.getGUI("particlesGUI");
            g.addItem("particlesGUI." + ParticleType.SPIRAL, "particlesGUI.spiralParticles");
            g.addItem("particlesGUI." + ParticleType.ANGRY_VILLAGER, "particlesGUI.angryParticles");
            g.addItem("particlesGUI." + ParticleType.DOUBLE_WITCH, "particlesGUI.doubleWitchParticles");
            g.addItem("particlesGUI." + ParticleType.NOTES, "particlesGUI.notesParticles");
            g.addItem("particlesGUI." + ParticleType.MAGIC, "particlesGUI.magicParticles");
            g.addItem("particlesGUI." + ParticleType.HAPPY_VILLAGER, "particlesGUI.happyParticles");
            g.addItem("particlesGUI.backItem", "particlesGUI.backItem");
        }
    }

    public static void setupBoostersGUI() {
        Config c = new Config("boostersGUI", "plugins/" + VipFeatures.plugin.getName());
        VipFeatures.boostersGUI = c;
        /* Create trails inventory */
        PlayerGUI.setupGUI(c, "", "boostersGUI", "&8Coins Multipliers", 27);
        PlayerGUI.createItem(c, "boostersGUI." + BoosterType.BOOSTER_X_2, Material.IRON_INGOT, 0, 1, 11, false,
                "&cCoins Booster &dx2", Arrays.asList("", "&f{status}"), "boostersGUI.boosterX2");
        PlayerGUI.createItem(c, "boostersGUI." + BoosterType.BOOSTER_X_3, Material.GOLD_INGOT, 0, 1, 15, false,
                "&cCoins Booster &dx3", Arrays.asList("", "&f{status}"), "boostersGUI.boosterX3");
        PlayerGUI.createItem(c, "boostersGUI.backItem", Material.ARROW, 0, 1, 22, false,
                "&8Back", Arrays.asList("", "&fClick to go back!"), "boostersGUI.backItem");
        for (Language l : Language.getLanguages()) {
            CustomGUI g = l.getGUI("boostersGUI");
            g.addItem("boostersGUI." + BoosterType.BOOSTER_X_2, "boostersGUI.boosterX2");
            g.addItem("boostersGUI." + BoosterType.BOOSTER_X_3, "boostersGUI.boosterX3");
            g.addItem("boostersGUI.backItem", "boostersGUI.backItem");
        }
    }


    /**
     * Save a value
     * It saves it into the yml file automatically.
     *
     * @since 0.1
     */
    public void set(String path, Object value) {
        yml.set(path, value);
        save();
    }

    public YamlConfiguration getYml() {
        return yml;
    }

    /**
     * Save changes in the yml file
     *
     * @since 0.1
     */
    public void save() {
        try {
            yml.save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check if path exists
     */
    public boolean exists(String path) {
        return yml.get(path) != null;
    }

    /**
     * Get a string list.
     * Colors are translated.
     */
    public List<String> getList(String path) {
        return yml.getStringList(path).stream().map(s -> s.replace("&", "§")).collect(Collectors.toList());
    }

    public boolean getBoolean(String path) {
        return yml.getBoolean(path);
    }

    public int getInt(String path) {
        return yml.getInt(path);
    }
}
