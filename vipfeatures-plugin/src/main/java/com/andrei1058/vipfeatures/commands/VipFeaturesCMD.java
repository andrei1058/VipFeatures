package com.andrei1058.vipfeatures.commands;

import com.andrei1058.vipfeatures.VipFeatures;
import com.andrei1058.vipfeatures.configuration.ConfigPath;
import com.andrei1058.vipfeatures.configuration.Language;
import com.andrei1058.vipfeatures.configuration.Messages;
import com.andrei1058.vipfeatures.configuration.Permissions;
import com.andrei1058.vipfeatures.api.MiniGame;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class VipFeaturesCMD extends Command {

    public VipFeaturesCMD(String name, List<String> aliases, String description) {
        super(name);
        this.setAliases(aliases);
        this.setDescription(description);
    }

    @Override
    public boolean execute(CommandSender s, String st, String[] args) {
        if (s instanceof ConsoleCommandSender){
            s.sendMessage("This command is for players only!");
            return true;
        }
        Player p = (Player) s;
        if (VipFeatures.config.getBoolean(ConfigPath.BLOCK_CHANGES_DURING_THE_GAME)) {
            for (MiniGame m : VipFeatures.getMiniGames()) {
                if (m.isPlaying(p)) {
                    p.sendMessage(Language.getMsg(p, Messages.CANT_OPEN_GUI_WHILE_PLAYING));
                    return true;
                }
            }
        }
        Language.getPlayerLanguage(p).openGUI("mainGUI", p, Permissions.PERMISSION_RECEIVE_AND_OPEN_MAIN_GUI);
        return false;
    }
}
