package com.andrei1058.vipfeatures.database;

import com.andrei1058.vipfeatures.VipFeatures;
import com.andrei1058.vipfeatures.api.*;
import com.andrei1058.vipfeatures.configuration.Config;
import com.andrei1058.vipfeatures.configuration.ConfigPath;

import java.sql.*;
import java.util.UUID;

public class MySQL implements DatabaseAdapter {

    private Connection connection;
    private String host, database, user, pass;
    private Config config = VipFeatures.config;
    private VipFeatures plugin = VipFeatures.plugin;
    int port;
    boolean ssl;

    public MySQL() {
        this.host = config.getYml().getString(ConfigPath.DATABASE_HOST);
        this.database = config.getYml().getString(ConfigPath.DATABASE_NAME);
        this.user = config.getYml().getString(ConfigPath.DATABASE_USER);
        this.pass = config.getYml().getString(ConfigPath.DATABASE_PASSWORD);
        this.port = config.getYml().getInt(ConfigPath.DATABASE_PORT);
        this.ssl = config.getYml().getBoolean(ConfigPath.DATABASE_SSL);
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?user=" + user
                    + "&password=" + pass + "&useSSL=" + ssl);
        } catch (SQLException e) {
            e.printStackTrace();
            plugin.getLogger().severe("Can't connect to database: " + database);
            plugin.getLogger().severe(e.getMessage());
            VipFeatures.databaseAdapter = new SQLite();
        } finally {
            if (isConnected()) {
                connect();
                plugin.getLogger().info("Connected to database: " + database);
                setupBoostersTable();
                setupParticlesTable();
                setupSpellsTable();
                setupTrailsTable();
            }
        }
    }

    public boolean connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?autoReconnect=true&user=" + user
                    + "&password=" + pass + "&useSSL=" + ssl);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean isRemote() {
        return true;
    }

    @Override
    public void close() {
        if (!isConnected()) return;
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setupTrailsTable() {
        if (!isConnected()) connect();
        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS vipFeatures_trails (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
                    "uuid VARCHAR(36), selected VARCHAR(30));");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setupSpellsTable() {
        if (!isConnected()) connect();
        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS vipFeatures_spells (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
                    "uuid VARCHAR(36), selected VARCHAR(30));");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setupParticlesTable() {
        if (!isConnected()) connect();
        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS vipFeatures_particles (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
                    "uuid VARCHAR(36), selected VARCHAR(30));");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setupBoostersTable() {
        if (!isConnected()) connect();
        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS vipFeatures_boosters (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
                    "uuid VARCHAR(36), selected VARCHAR(30));");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public TrailType getSelectedTrails(UUID uuid) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT selected FROM vipFeatures_trails WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) return TrailType.valueOf(rs.getString("selected"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return TrailType.NONE;
    }

    @Override
    public SpellType getSelectedSpells(UUID uuid) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT selected FROM vipFeatures_spells WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) return SpellType.valueOf(rs.getString("selected"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return SpellType.NONE;
    }

    @Override
    public ParticleType getSelectedParticles(UUID uuid) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT selected FROM vipFeatures_particles WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) return ParticleType.valueOf(rs.getString("selected"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ParticleType.NONE;
    }

    @Override
    public BoosterType getSelectedBooster(UUID uuid) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT selected FROM vipFeatures_boosters WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) return BoosterType.valueOf(rs.getString("selected"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return BoosterType.NONE;
    }

    @Override
    public void setSelectedBooster(UUID uuid, BoosterType boosterType) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM vipFeatures_boosters WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) {
                connection.createStatement().executeUpdate("UPDATE vipFeatures_boosters SET selected='" + boosterType.toString() + "' WHERE uuid='" + uuid.toString() + "';");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO vipFeatures_boosters VALUES(0, '" + uuid.toString() + "', '" + boosterType.toString() + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSelectedParticles(UUID uuid, ParticleType particlesType) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM vipFeatures_particles WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) {
                connection.createStatement().executeUpdate("UPDATE vipFeatures_particles SET selected='" + particlesType.toString() + "' WHERE uuid='" + uuid.toString() + "';");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO vipFeatures_particles VALUES(0, '" + uuid.toString() + "', '" + particlesType.toString() + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSelectedSpells(UUID uuid, SpellType spellType) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM vipFeatures_spells WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) {
                connection.createStatement().executeUpdate("UPDATE vipFeatures_spells SET selected='" + spellType.toString() + "' WHERE uuid='" + uuid.toString() + "';");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO vipFeatures_spells VALUES(0, '" + uuid.toString() + "', '" + spellType.toString() + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSelectedTrails(UUID uuid, TrailType trailType) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM vipFeatures_trails WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) {
                connection.createStatement().executeUpdate("UPDATE vipFeatures_trails SET selected='" + trailType.toString() + "' WHERE uuid='" + uuid.toString() + "';");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO vipFeatures_trails VALUES(0, '" + uuid.toString() + "', '" + trailType.toString() + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return connection != null;
    }
}
