package com.andrei1058.vipfeatures.database;

import com.andrei1058.vipfeatures.VipFeatures;
import com.andrei1058.vipfeatures.api.*;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.UUID;

public class SQLite implements DatabaseAdapter {

    private Connection connection;

    public SQLite() {
        File folder = VipFeatures.plugin.getDataFolder();
        if (!folder.exists()) {
            if (!folder.mkdir()) {
                VipFeatures.plugin.getLogger().severe("Could not create plugin folder!");
            }
        }
        File dataFolder = new File(folder.getPath() + "/local-database.db");
        if (!dataFolder.exists()) {
            try {
                if (!dataFolder.createNewFile()) {
                    VipFeatures.plugin.getLogger().severe("Could not create /Cache/shop.db file!");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        try {
            ClassLoader cl = Bukkit.getServer().getClass().getClassLoader();
            Driver d = (Driver) cl.loadClass("org.sqlite.JDBC").newInstance();
            DriverManager.registerDriver(d);
            connection = DriverManager.getConnection("jdbc:sqlite:" + dataFolder);
        } catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            if (e instanceof ClassNotFoundException) {
                VipFeatures.plugin.getLogger().severe("Could Not Found SQLite Driver on your system!");
            }
            e.printStackTrace();
            VipFeatures.databaseAdapter = new None();
            return;
        }

        VipFeatures.plugin.getLogger().info("Using SQLite adapter.");
        setupBoostersTable();
        setupParticlesTable();
        setupSpellsTable();
        setupTrailsTable();
    }

    @Override
    public boolean isRemote() {
        return false;
    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException ignored) {
        }
    }

    @Override
    public void setupTrailsTable() {
        String sql = "CREATE TABLE IF NOT EXISTS vipFeatures_trails (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "uuid VARCHAR(36), selected VARCHAR(30));";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setupSpellsTable() {
        String sql = "CREATE TABLE IF NOT EXISTS vipFeatures_spells (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "uuid VARCHAR(36), selected VARCHAR(30));";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setupParticlesTable() {
        String sql = "CREATE TABLE IF NOT EXISTS vipFeatures_particles (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "uuid VARCHAR(36), selected VARCHAR(30));";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setupBoostersTable() {
        String sql = "CREATE TABLE IF NOT EXISTS vipFeatures_boosters (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "uuid VARCHAR(36), selected VARCHAR(30));";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TrailType getSelectedTrails(UUID uuid) {
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT selected FROM vipFeatures_trails WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) return TrailType.valueOf(rs.getString("selected"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return TrailType.NONE;
    }

    @Override
    public SpellType getSelectedSpells(UUID uuid) {
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT selected FROM vipFeatures_spells WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) return SpellType.valueOf(rs.getString("selected"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return SpellType.NONE;
    }

    @Override
    public ParticleType getSelectedParticles(UUID uuid) {
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT selected FROM vipFeatures_particles WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) return ParticleType.valueOf(rs.getString("selected"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ParticleType.NONE;
    }

    @Override
    public BoosterType getSelectedBooster(UUID uuid) {
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT selected FROM vipFeatures_boosters WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) return BoosterType.valueOf(rs.getString("selected"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return BoosterType.NONE;
    }

    @Override
    public void setSelectedBooster(UUID uuid, BoosterType boosterType) {
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM vipFeatures_boosters WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) {
                connection.createStatement().executeUpdate("UPDATE vipFeatures_boosters SET selected='" + boosterType.toString() + "' WHERE uuid='" + uuid.toString() + "';");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO vipFeatures_boosters VALUES(0, '" + uuid.toString() + "', '" + boosterType.toString() + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSelectedParticles(UUID uuid, ParticleType particlesType) {
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM vipFeatures_particles WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) {
                connection.createStatement().executeUpdate("UPDATE vipFeatures_particles SET selected='" + particlesType.toString() + "' WHERE uuid='" + uuid.toString() + "';");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO vipFeatures_particles VALUES(0, '" + uuid.toString() + "', '" + particlesType.toString() + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSelectedSpells(UUID uuid, SpellType spellType) {
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM vipFeatures_spells WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) {
                connection.createStatement().executeUpdate("UPDATE vipFeatures_spells SET selected='" + spellType.toString() + "' WHERE uuid='" + uuid.toString() + "';");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO vipFeatures_spells VALUES(0, '" + uuid.toString() + "', '" + spellType.toString() + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSelectedTrails(UUID uuid, TrailType trailType) {
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM vipFeatures_trails WHERE uuid='" + uuid.toString() + "';");
            if (rs.next()) {
                connection.createStatement().executeUpdate("UPDATE vipFeatures_trails SET selected='" + trailType.toString() + "' WHERE uuid='" + uuid.toString() + "';");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO vipFeatures_trails VALUES(0, '" + uuid.toString() + "', '" + trailType.toString() + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
