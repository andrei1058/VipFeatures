package com.andrei1058.vipfeatures.database;

import com.andrei1058.vipfeatures.api.*;

import java.util.UUID;

public class None implements DatabaseAdapter {
    @Override
    public boolean isRemote() {
        return false;
    }

    @Override
    public void close() {

    }

    @Override
    public void setupTrailsTable() {

    }

    @Override
    public void setupSpellsTable() {

    }

    @Override
    public void setupParticlesTable() {

    }

    @Override
    public void setupBoostersTable() {

    }

    @Override
    public TrailType getSelectedTrails(UUID uuid) {
        return TrailType.NONE;
    }

    @Override
    public SpellType getSelectedSpells(UUID uuid) {
        return SpellType.NONE;
    }

    @Override
    public ParticleType getSelectedParticles(UUID uuid) {
        return ParticleType.NONE;
    }

    @Override
    public BoosterType getSelectedBooster(UUID uuid) {
        return BoosterType.NONE;
    }

    @Override
    public void setSelectedBooster(UUID uuid, BoosterType boosterType) {

    }

    @Override
    public void setSelectedParticles(UUID uuid, ParticleType particlesType) {

    }

    @Override
    public void setSelectedSpells(UUID uuid, SpellType spellType) {

    }

    @Override
    public void setSelectedTrails(UUID uuid, TrailType trailType) {

    }
}
